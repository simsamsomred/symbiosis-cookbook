"""
Pandoc filter to apply mustache templates on regular text.

Original code is here, 

https://github.com/michaelstepner/pandoc-mustache/

but the repo was no longer maintained. I had to add the 
parsing of image files to be able to handle this properly.
"""
from past.builtins import basestring
from panflute import *
import pystache, yaml

def prepare(doc):
    """ Parse metadata to obtain list of mustache templates,
        then load those templates.
    """
    # with open('debug.txt', 'a') as the_file:
    #     the_file.write(str(doc.get_metadata('mustache')))
    #     the_file.write('\n')
    doc.mustache_files = doc.get_metadata('mustache')
    if isinstance(doc.mustache_files, basestring):  # process single YAML value stored as string
        if not doc.mustache_files:
            doc.mustache_files = None  # switch empty string back to None
        else:
            doc.mustache_files = [ doc.mustache_files ]  # put non-empty string in list
    
    if doc.mustache_files is not None:
        doc.mustache_hashes = [yaml.load(open(file, 'r').read(), Loader=yaml.SafeLoader) for file in doc.mustache_files]
        doc.mhash = { k: v for mdict in doc.mustache_hashes for k, v in mdict.items() }  # combine list of dicts into a single dict

        doc.mrenderer = pystache.Renderer(escape=lambda u: u, missing_tags='strict')
    else:
        doc.mhash = None

def action(elem, doc):
    """ Apply combined mustache template to all strings in document.
    """
    if type(elem) == Str and doc.mhash is not None:
        elem.text = doc.mrenderer.render(elem.text, doc.mhash)
        return elem

    if type (elem) == Image and doc.mhash is not None: 
        elem.url = doc.mrenderer.render(
            elem.url
                .replace('%7B%7B', '{{')
                .replace('%7D%7D', '}}'), 
            doc.mhash)
        return elem


def main(doc=None):
    return run_filter(action, prepare=prepare, doc=doc)

if __name__ == '__main__':
    main()
