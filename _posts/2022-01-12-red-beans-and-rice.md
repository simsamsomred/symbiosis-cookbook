---
layout: post
title: Red Beans and Rice
date: 2022-01-12 15:40
category: dinner
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Red Beans and Rice

Recipe serves 6

## Ingredients

- 1 lb dry red kidney beans, soaked overnight
- 2 tablespoons cooking oil
- 1 medium yellow onion, diced
- 1 green bell pepper, cored and diced
- 2 medium stalks celery
- 6 cloves garlic, minced
- 2 tablespoons fresh parsley
- Salt and pepper to taste
- 2 bay leaves
- 32 oz vegetable broth
- 1 ½ cups dry brown rice

## Instructions

1. Drain the kidney beans; set aside.
2. In a large pot over medium heat, add oil. When hot, add onion, bell pepper and celery. Cook until vegetables are tender and slightly browned, about 8-10 minutes. Add garlic; cook for one more minute, until fragrant.
3. Stir in parsley, hot sauce, thyme, paprika, salt and pepper; stir to evenly coat the vegetables in the spices. Cook for one minute.
4. Pour in kidney beans, bay leaves and vegetable broth. Bring to a boil; cover, lower heat and simmer for 1 hour and 15 minutes. Remove lid; let simmer uncovered for 15 minutes.
5. While the beans are cooking, prepare rice according to package directions.
6. When the beans are ready, scoop about 1/4 of the beans in a blender or food processor; blend until smooth. Return to pot; add liquid smoke (if using). Stir
7. Serve beans with a scoop of rice and a sprinkle of chopped parsley.
