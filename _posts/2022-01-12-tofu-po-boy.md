---
layout: post
title: Tofu Po'Boy
date: 2022-01-12 17:18
category: dinner
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Tofu Po'Boy

Recipe serves 6

## Ingredients

- 1 Cabbage
- 2 baguettes
- 3 tomatoes, sliced
- 28 oz super firm tofu
- ½ cup nutritional yeast
- 4 tbsp soy sauce
- ½ cup water
- 2 tbsp garlic powder
- 2 tsp onion powder
- 2 tsp paprika
- 2 tsp veggie broth powder
- ½ cup vegan oil-free vegan aioli
- ½ cup unsweetened vegan yogurt
- 2 tbsp capers
- 1 tbsp fresh parsley
- 1 tbsp brown mustard
- 1 tsp lemon juice
- 1 tsp garlic powder
- ½ tsp smoked paprika
- ½ tsp sea salt
- ½ tsp hot sauce

## Tofu Nuggets

1. Preheat oven to 425°F or 220°C.
2. In a large glass mixing bowl, combine all of the "Paste" ingredients and stir to combine. This should create a thick-ish paste. If the mixture is too thin, add more nutritional yeast. If the mixture is too thick, add water in small increments.
3. Unwrap tofu and break into bite-sized chunks over the bowl. As you do this, use your thumb to create craggy, rounded edges. (They're still edible if they're blocky and smooth, but the pieces will hold the paste better and take on a more nugget-like shape if they're rounded and have craggy edges.)
4. Lightly fold the chunks into the paste, being careful not to break the tofu too much.
5. Scoop tofu onto a lined sheet pan and place in the oven. Cook for 30 minutes or until the tofu is nicely browned on the outside.

## Vegan Remoulade

1. Chop the capers and parsley.
2. Add all of the ingredients to a bowl and stir to combine.
3. Taste to adjust seasonings. Serve right away or store for later.

## Sandwich

1. Start by making the tofu nuggets following the linked instructions.
2. While the tofu nuggets are in the oven, make the remoulade and prep the veggies.
3. When the tofu nuggets are done, assemble the sandwiches. Add 2 tablespoons of remoulade, 3 tomato slices, and a handful of lettuce. Top with tofu nuggets.
4. Serve right away and enjoy. Or keep the ingredients separate to assemble later.
