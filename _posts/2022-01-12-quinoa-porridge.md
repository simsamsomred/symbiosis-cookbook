---
layout: post
title: Quinoa Porridge
date: 2022-01-12 15:35
category: breakfast
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Quinoa Porridge

Recipe serves 6

## Ingredients

- 2 cups of quinoa, rinsed
- 4 cups of coconut milk
- 3 teaspoons cinnamon
- 3 teaspoons vanilla extract
- 6 tablespoons maple syrup
- 1 teaspoon salt

## Toppings

- Peach slices
- Almonds
- Maple syrup

## Instructions

1. Rinse the quinoa using a fine mesh strainer.
2. Bring milk to a low boil, add the quinoa and bring back to a boil. Reduce heat, cover with lid and simmer for 15-20 minutes until most of the milk has been absorbed.

3. Stir in the cinnamon, salt, vanilla and maple syrup until fully combined.
