---
layout: post
title: Vegan Shakshuka
date: 2022-01-12 15:37
category: breakfast
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Vegan Shakshuka

Recipe serves 6

## Ingredients

- 3 cans of diced tomatoes
- 3 tablespoons of minced garlic
- 1 medium onion
- 1 medium bell pepper
- 2 teaspoons cumin
- 1 teaspoon red pepper flakes
- 2 teaspoons oregano
- 1 teaspoon salt
- 1 teaspoon black pepper
- 24 oz of extra firm tofu (pressed for at least 30 minutes)
- Pinch of salt and pepper
- 2 teaspoon garlic powder
- 3 tablespoons nutritional yeast
- 1 ½ cups soy milk

## Toppings

- Vegan feta cheese
- Cilantro

## Instructions

1. Heat oil over medium high heat, add onions and bell peppers and cook until translucent (5-7 minutes).

2. While this is cooking, make the tofu egg mixture. Add the tofu, nutritional yeast, salt and pepper, garlic powder, onion powder, milk and blend. Set aside until needed.

3. Add garlic to skillet and stir for about 2 minutes until fragrant.

4. Add tomatoes, cumin and red pepper flakes, as well as a pinch of salt and ground black pepper. Reduce heat to medium and stir everything together. Cover with a lid and cook for another 2 minutes.

5. Remove lid, make some small wells, and scoop in tofu egg mixture. Five on the outside, one in the middle.

6. Bring heat to medium and cover skillet with the lid. Cook for 5 minutes with the loon on to cook through, then 5 to 8 minutes with the lid off, allowing all the excess liquid to escape until the egg mixture is set.

7. You can prep the tofu egg mixture the day before. Just store it in your fridge, then bring to room temp for 30 minutes before you prepare the rest of the dish.
