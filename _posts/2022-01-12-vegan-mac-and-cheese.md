---
layout: post
title: Vegan Mac and Cheese
date: 2022-01-12 17:20
category: dinner
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Vegan Mac and Cheese

Recipe serves 6

## Ingredients

- \*½ cup sweet potato
- 1 cup potato
- ½ cup carrots
- 16 oz dried elbow macaroni
- 1 cup unsweetened soy milk
- 1 cup vegetable broth
- 1 teaspoon onion powder
- 1 teaspoon salt
- ½ teaspoon paprika
- 3 tablespoons nutritional yeast
- 3 tablespoons tapioca flour/starch
- 2 tablespoons fresh lemon juice
- 2 tablespoons vegan butter
- 2 cloves garlic

## Instructions

1. Boil the cashews, potatoes, and carrots until tender (about 8-10 minutes). Drain well and set aside.
2. Place macaroni in a large pot of salted boiling water. Cook until al dente and according to package directions (about 9-10 minutes). Drain and set aside.
3. Place all ingredients in a high-powered blender (except garlic & butter). Blend until potatoes, carrots & cashews have completely broken down and sauce is smooth, about 1-2 minutes. Set aside.
4. Heat up butter in a large pan over medium heat. When melted, add garlic and sauté for 30-60 seconds, until lightly golden and fragrant (be careful not to burn).
5. Pour in the cheese sauce and cook for 4-6 minutes until sauce thickens up and becomes slightly stretchy. Stir often to prevent burning. If it's too thick, add a splash of broth. Taste for seasoning and add more if needed.
   Now add pasta and gently toss to coat. Serve immediately. Enjoy!

## Notes

- I usually start blending the sauce 5 minutes before the pasta will be done. This way the pasta isn't sitting too long, and it will be hot & fresh when it goes into the sauce.
- To make it easier, cook potatoes, cashews and carrots the night before and pop them in the fridge.
- If you'd like a thinner cheese sauce, use only 2 tablespoons of tapioca starch, or omit it altogether. The sauce will still be rich and creamy. Tapioca starch provides extra thickness and a slight "cheesy" stretch.
- The cheese has a very smooth and creamy texture. So, if it comes out a bit grainy, you may need to blend longer. If it's still grainy, your blender may not be strong enough to pulverize the cashews completely smooth.
