---
layout: post
title: Tofu Scramble
date: 2022-01-12 15:37
category: breakfast
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Tofu Scramble

Recipe serves 6

## Ingredients

- ½ 16 oz block firm tofu
- 1 tablespoon unflavored and unsweetened non-dairy milk
- ½ tablespoon olive oil
- 1 tablespoon nutritional yeast
- ¼ teaspoon turmeric
- ¼ teaspoon garlic powder
- Pinch of salt

## Toppings

- Vegan cheddar cheese
- Avocado
- Hot sauce

## Instructions

1. Heat oil in pan over medium heat. Mash block of tofu right in the pan with a masher or fork. Cook, stirring frequently, for 3-4 minutes until water from tofu is mostly gone.
2. Add yeast, salt, turmeric and garlic powder. Cook and stir constantly for about 5 minutes. Our non-dairy milk into pan and stir to mix.
