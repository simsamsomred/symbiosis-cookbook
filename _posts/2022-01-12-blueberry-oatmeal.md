---
layout: post
title: "Blueberry Oatmeal"
date: 2022-01-12 15:33
category: breakfast
author:
tags: ["symbiosis summer 2021", vegan]
mustache: "./pandoc-mustache.yml"
summary:
---

# Blueberry Oatmeal

Recipe serves 6

![plates]({{site.baseurl}}/assets/images/plates.png){: .float-right}

## Ingredients

- 2 cups of old fashioned/rolled oats
- 3 cups of coconut milk
- 3 tablespoons coconut oil
- 8 cups blueberries
- ⅔ cup brown sugar
- 3 teaspoons cinnamon
- 2 teaspoons nutmeg
- 1 teaspoon salt

## Toppings

- Blueberries
- Walnuts
- Maple syrup

## Instructions

1. Heat oil over medium high heat, add blueberries and brown sugar and sprinkle in spices and salt. Cook for 2-3 minutes until blueberries are softened.
2. Pour in the milk and turn up until it begins to low boil, stirring regularly. Add the oats and reduce the heat to simmer, stirring occasionally for 3-5 minutes, or until most of the liquid is absorbed.
