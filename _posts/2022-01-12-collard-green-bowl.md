---
layout: post
title: "Collard Green Bowl"
date: 2022-01-12 15:43
category: dinner
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Collard Green Bowl

Recipe serves 6

## Ingredients

- 3 bunches collard greens
- 6 tablespoons grapeseed oil
- 16 oz sweet potatoes peeled and cut
- 1 sliced yellow onion
- 12 cloves garlic, minced
- 6 tablespoons red wine vinegar
- 1 teaspoon sea salt
- 1 teaspoon black pepper
- ½ teaspoon red pepper flakes
- 1 ½ cups dry brown rice
- 2 cups pickled red onion
- 2 green onions
- 1 pound dried black eyed peas
- 32 oz vegetable broth
- 1 teaspoon salt
- 1 teaspoon dried parsley
- 1 teaspoon paprika
- 1 teaspoon cumin
- 1 teaspoon oregano

## Instructions

1. Separate the collard green stems from the leaves. Dice the stems and set aside. Roughly chop the leaves.
2. Heat the grapeseed oil in a large skillet set over medium heat. Once the oil is shimmering, add the collard green stems along with the sliced yellow onion; saute for 2 to 3 minutes, until the onions are lightly browned. Add the garlic to the pan and saute for an additional minute.
3. Add the chopped leaves to the pan along with the red wine vinegar, salt, pepper, red pepper flakes, and 2 tablespoons of water.
4. Toss the collard greens to coat the leaves in the pan liquid, then cover the pan and turn the heat down to medium low. Allow the greens to steam, stirring occasionally, until the leaves are bright green and tender (about 4 to 5 minutes).
5. Divide the rice, black eyed peas, and collard greens into four bowls. Top each with a quarter of the red onion and zucchini pickles. Garnish with sliced green onion and serve warm.
