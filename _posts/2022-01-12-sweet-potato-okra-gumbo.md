---
layout: post
title: Sweet Potato, Okra, and Chickpea Gumbo
date: 2022-01-12 15:47
category: dinner
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Sweet Potato, Okra, and Chickpea Gumbo

Recipe serves 6

## Ingredients

- 1 large yellow onion chopped
- 1 large green pepper chopped
- 2 ribs celery chopped
- 4 cloves garlic
- 8 cups vegetable broth
- 2 15 oz can diced tomatoes (fire-roasted preferred)
- 1 ½ cups cooked chickpeas
- 16 oz sweet potatoes peeled and cut
- 16 oz okra trimmed and sliced
- 1 teaspoon salt
- 1 teaspoon black pepper
- 2 bay leaves
- ¼ teaspoon cayenne pepper
- 1 teaspoon paprika
- 1 tablespoon sunflower butter
- 1 ½ cups dry brown rice

## Instructions

1. Heat a large pot. Add the onions and a pinch of baking soda (optional but speeds browning). Cook, stirring, until onions brown, adding water by the teaspoon if needed to prevent sticking.
2. Add the pepper, celery, and garlic and cook for 2 more minutes.
3. Add 7 cups water or vegetable broth and tomatoes, stirring to combine. Then add all the remaining ingredients except the liquid smoke and peanut butter. Simmer uncovered until sweet potatoes are tender and just close to falling apart, at least an hour to give flavors a chance to mingle, adding extra water or broth if needed.
4. Just before serving, stir in the liquid smoke and peanut butter (it helps if you mix the peanut butter with a couple tablespoons of the hot broth first). Remove bay leaves and serve over rice.
