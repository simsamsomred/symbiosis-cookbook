---
layout: post
title: Banana Bread Oatmeal
date: 2021-11-19 09:24:51 -0800
category: breakfast
tags: ["symbiosis summer 2021", vegan]
---

# Banana Bread Oatmeal

Recipe serves 6

## Ingredients

- 2 cups of old fashioned/rolled oats
- 6 bananas (mashed)
- 3 cups of coconut milk
- 3 teaspoons of cinnamon
- 6 teaspoons of chia seeds
- 1 teaspoon of salt

## Toppings

- Banana
- Walnuts
- Maple syrup

## Instructions

1. Combine oats, banana, soy milk, cinnamon and chia seeds and simmer for 5-8 minutes, stirring occasionally.

2. Serve in bowl and spoon with toppings.
