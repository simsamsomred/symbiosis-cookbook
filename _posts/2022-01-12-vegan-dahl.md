---
layout: post
title: Vegan Dahl
date: 2022-01-12 15:46
category: dinner
author:
tags: ["symbiosis summer 2021", vegan]
summary:
---

# Vegan Dahl

Recipe serves 6

## Ingredients

- 1 tablespoon olive oil
- 1 large yellow onion
- 4 cloves garlic
- 1 tablespoon ginger
- 1 tablespoon garam masala
- 1 teaspoon turmeric
- ½ teaspoon red pepper flakes
- 1 ½ cupps dried red lentils
- 14 oz can diced tomatoes
- 13.5 oz can full fat coconut milk
- 3 cups vegetable broth
- 1 teaspoon salt
- Half a lemon, juiced
- ¾ cups baby spinach
- 1 ½ cups dry brown rice

## Instructions

1. In a large pot or pan over medium heat, sauté the chopped onion in the olive oil for 5 minutes, stirring frequently. Then add the garlic and ginger and cook 1 more minute, until fragrant.
2. Add the garam masala, turmeric and red pepper flakes to the pan and stir into the onion mixture. Add a few tablespoons of water if the mixture is too dry.
3. Now add the dried lentils, canned tomatoes and their juices, coconut milk and vegetable broth to the pan. Stir well and turn the heat to high. Bring to a boil, then lower heat and simmer for about 15 minutes, until the lentils are cooked and soft. Stir occasionally.
4. Squeeze the lemon juice into the pan, and stir in the spinach as well until wilted. Add salt to taste. I used 1 teaspoon.
5. Serve with brown or white rice and Vegan Naan. Enjoy!
