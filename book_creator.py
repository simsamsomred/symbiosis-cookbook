from ruamel.yaml import YAML
from pathlib import Path
import os
import pypandoc
from collections import OrderedDict
import subprocess

directory = "_posts"
files = os.listdir(directory)
ordered = OrderedDict()
for mdfile in files:
  
    if mdfile.endswith('.md'):
        path = Path(directory + "/" + mdfile)

        yaml = YAML()   # default, if not specfied, is 'rt' (round-trip)
        for data in yaml.load_all(path):
            ordered[mdfile] = data
            break

sorted_dict = OrderedDict(sorted(ordered.items(), key=lambda x: x[1]['category']))
sorted_files = list(map(lambda x: directory + "/" + x[0], sorted_dict.items()))

subprocess.check_call(['pandoc', 
    *sorted_files,
    '--from', 'markdown', 
    '--filter', 'scripts/pandoc_mustache.py', 
    '-M', 'title="Symbiosis Summer Cookbook"',
    '-M', 'author="Symbiosis Volunteers"',
    '-o', 'books/symbiosis-summer-cookbook.epub'])
