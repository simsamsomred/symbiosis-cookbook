---
layout: page
title: About
permalink: /about/
---

This site contains the recipes we cooked at Symbiosis Summer to feed ourselves while we worked and learned.

It's a very simple jekyll site, which you can help maintain [on gitlab](https://gitlab.com/simsamsomred/symbiosis-cookbook).
