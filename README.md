# Symbiosis Cookbook

This is a [jekyll](https://jekyllrb.com) powered website.

```
git clone <url>
jekyll serve
```

## Ebook

### Directly with Pandoc

> Hopefully soon to be easier and less elaborate.

To generate the e-pub, you'll need to have [pandoc installed](https://pandoc.org/installing.html). You'll need a version of pandoc that is more modern than the one automatically installed by the Ubuntu distro.

In this directory run

```
python3 -m venv .venv
. .venv/bin/activate
```

to create and activate a virtual environment.

Then run

```
pip install -r requirements.txt
```

To install all the requirements.

Then:

```
python book_creator.py
```
